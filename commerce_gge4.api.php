<?php

/**
 * @file
 * Hooks provided by the Commerce GGe4 payments module.
 */

/**
 * Alter the amount to be authorized during a pre-authorization
 * transaction that does not also capture the charge.
 *
 * @param array $charge Charge array provided by commerce_payment
 */
function hook_commerce_gge4_capture_charge_alter(&$charge) {
  // Auth for 10% more than the order amount.
  $charge['amount'] = $charge['amount'] * 1.1;
}
