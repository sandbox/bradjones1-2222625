Integration with First Data's Commerce GGe4 payments API

Dependencies
* This module requires commerce, commerce_ui, commerce_payment, and commerce_order.

Other requirements
* You must have at least a developer account, available at
  https://firstdata.zendesk.com/entries/21510561-first-data-global-gateway-e4sm-demo-accounts

API documentation
* See https://firstdata.zendesk.com/

Installation
* Copy this module into your modules directory, enable it, and configure the
  payment rule provided by this module to use your production or development
  API key.
